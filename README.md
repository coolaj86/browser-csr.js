browser-csr
===========

Generate a Certificate Signing Request (CSR) in your browser!

Used for Let's Encrypt v2 with [greenlock.html](https://git.coolaj86.com/coolaj86/greenlock.html).

Uses as few dependencies as possible from the PKI.js and ASN1.js suites and supports SAN AltNames.

Usage
-----

```html
<script src="/js/browser-csr.js"></script>
```

```js
CSR.generate({

  /* A WebCrypto-generated keypair */
  keypair: {}

  /* Subject & SANS altnames */
, domains: [ 'example.com', 'www.example.com' ]

, format: 'rfc7515' // unpadded urlsafe base64

}).then(function (csrweb64) {
  console.log(csrweb64);
});
```

Dependencies
------------

[`pkijs@v1.3.33`](https://github.com/PeculiarVentures/PKI.js/tree/41b63af760cacb565dd850fb3466ada4ca163eff)

```
<script src="https://coolaj86.com/assets/media/examples/pkijs.org/v1.3.33/org/pkijs/common.js"></script>
<script src="https://coolaj86.com/assets/media/examples/pkijs.org/v1.3.33/org/pkijs/asn1.js"></script>
<script src="https://coolaj86.com/assets/media/examples/pkijs.org/v1.3.33/org/pkijs/x509_schema.js"></script>
<script src="https://coolaj86.com/assets/media/examples/pkijs.org/v1.3.33/org/pkijs/x509_simpl.js"></script>
```
